import { CodeBucketError } from '../error';
import { IssueTracker } from '../issuetracker/issuetracker';
import { JiraIssueTracker } from '../issuetracker/issuetracker-jira';
import { issueTrackers } from '../settings';

export interface HostConfig {
  name: string;
  webHost: string;
  gitHost: string;
  repo: string;
}

export abstract class Host {
  constructor(
    private readonly cfg: HostConfig) {
  }

  get name(): string {
    return this.cfg.name;
  }

  get webHost(): string {
    return this.cfg.webHost;
  }

  get gitHost(): string {
    return this.cfg.gitHost;
  }

  get repo(): string {
    return this.cfg.repo;
  }

  public abstract getChangeSetUrl(revision: string, filePath: string): string;
  public abstract getSourceUrl(revision: string, filePath: string, lineRanges: string[]): string;
  public abstract getPullRequestUrl(id: number, filePath: string): string;

  /**
   * Returns all configured issue trackers
   */
  public async getIssueTrackers(): Promise<IssueTracker[]> {

    const trackers: IssueTracker[] = issueTrackers().map(tracker => {
      switch (tracker.type) {
        case 'jira': return new JiraIssueTracker(tracker);
        default: throw new CodeBucketError(`Unknown issue tracker type: ${tracker.type}`);
      }
    });

    return trackers;
  }
}
