import * as vscode from 'vscode';

import { OpenInBitbucketCommand } from './command/command-open';
import { OpenBitbucketChangesetCommand } from './command/command-open-changeset';
import { OpenInIssueTrackerCommand } from './command/command-open-issue';
import { OpenBitbucketPullRequestCommand } from './command/command-open-pullrequest';

export function activate(context: vscode.ExtensionContext) {

    const openInBitbucket = new OpenInBitbucketCommand();
    const openInBitbucketCmd = vscode.commands.registerCommand('codebucket.open', () => openInBitbucket.run());
    context.subscriptions.push(openInBitbucketCmd);

    const openChangeset = new OpenBitbucketChangesetCommand();
    const openChangesetCmd = vscode.commands.registerCommand('codebucket.openChangeset', () => openChangeset.run());
    context.subscriptions.push(openChangesetCmd);

    const openPullRequest = new OpenBitbucketPullRequestCommand();
    const openPullRequestCmd = vscode.commands.registerCommand('codebucket.openPullRequest', () => openPullRequest.run());
    context.subscriptions.push(openPullRequestCmd);

    const openIssue = new OpenInIssueTrackerCommand();
    const openIssueCmd = vscode.commands.registerCommand('codebucket.openIssue', () => openIssue.run());
    context.subscriptions.push(openIssueCmd);

}
