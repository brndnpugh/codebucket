export interface IssueTracker {
  /**
   * Extract issue key from a commit message and return the issue tracker URL
   */
  findIssueUrl(message: string): string | undefined;
}
